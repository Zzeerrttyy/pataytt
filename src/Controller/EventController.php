<?php

namespace App\Controller;

use App\Entity\Event;
use App\Repository\EventRepository;
use Doctrine\ORM\Mapping\OrderBy;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;

class EventController extends AbstractController
{
    #[Route('/evenements-tennis-de-table', name: 'app_event')]
    public function index(EventRepository $er): Response
    {
        return $this->render('event/index.html.twig', [
            'events' => $er->findBy([], [ 'startAt' => 'DESC']),
        ]);
    }

    #[Route('/evenements-tennis-de-table/{id}', name: 'app_event_view')]
    public function view(Event $event): Response
    {
        return $this->render('event/show.html.twig', [
            'event' => $event,
        ]);
    }
    
}
