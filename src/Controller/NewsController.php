<?php

namespace App\Controller;

use App\Entity\News;
use App\Repository\NewsRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;

class NewsController extends AbstractController
{
    #[Route('/actus-ping-pong-patay', name: 'app_news')]
    public function index(NewsRepository $nr): Response
    {
        return $this->render('news/index.html.twig', [
            'news' => $nr->findAll(),
        ]);
    }

    #[Route('/actus-ping-pong-patay/{id}', name: 'app_news_show')]
    public function show(News $news): Response
    {
        return $this->render('news/show.html.twig', [
            'news' => $news,
        ]);
    }
}
