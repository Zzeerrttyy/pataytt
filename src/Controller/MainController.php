<?php

namespace App\Controller;

use App\Entity\Contact;
use App\Form\ContactType;
use App\Repository\NewsRepository;
use App\Repository\EventRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class MainController extends AbstractController
{
    #[Route('/', name: 'app_main')]
    public function index(Request $request, NewsRepository $nrepo, EventRepository $erepo)
    {
        $lastNews = $nrepo->findBy([], ['createdAt' => 'DESC'], 4);
        $events = $erepo->findBy([], [ 'startAt' => 'DESC']);

        return $this->render('main/index.html.twig', [
            'news' => $lastNews,
            'events' => $events
        ]);
    }

    #[Route('/le-club-ping-pong-patay', name: 'app_le_club')]
    public function leClub(Request $request, NewsRepository $nrepo, EventRepository $erepo)
    {
        $lastNews = $nrepo->findBy([], ['createdAt' => 'DESC'], 4);
        $events = $erepo->findBy([], [ 'startAt' => 'DESC']);

        return $this->render('main/club/le_club.html.twig', [
            'news' => $lastNews,
            'events' => $events
        ]);
    }

    #[Route('/inscription-tennis-de-table', name: 'app_inscription')]
    public function inscription(Request $request, NewsRepository $nrepo, EventRepository $erepo)
    {
        $lastNews = $nrepo->findBy([], ['createdAt' => 'DESC'], 4);
        $events = $erepo->findBy([], [ 'startAt' => 'DESC']);

        return $this->render('main/club/inscription.html.twig', [
            'news' => $lastNews,
            'events' => $events
        ]);
    }

    #[Route('/horaires-entrainements-ping-pong', name: 'app_horaires')]
    public function horaires(Request $request, NewsRepository $nrepo, EventRepository $erepo)
    {
        $lastNews = $nrepo->findBy([], ['createdAt' => 'DESC'], 4);
        $events = $erepo->findBy([], [ 'startAt' => 'DESC']);

        return $this->render('main/club/horaires.html.twig', [
            'news' => $lastNews,
            'events' => $events
        ]);
    }

    #[Route('/partenaires-et-sponsors', name: 'app_partenaires')]
    public function partenaires(Request $request, NewsRepository $nrepo, EventRepository $erepo)
    {
        $lastNews = $nrepo->findBy([], ['createdAt' => 'DESC'], 4);
        $events = $erepo->findBy([], [ 'startAt' => 'DESC']);

        return $this->render('main/club/partenaires.html.twig', [
            'news' => $lastNews,
            'events' => $events
        ]);
    }

    #[Route('/la-salle-de-tennis-de-table', name: 'app_lasalle')]
    public function laSalle(Request $request, NewsRepository $nrepo, EventRepository $erepo)
    {
        $lastNews = $nrepo->findBy([], ['createdAt' => 'DESC'], 4);
        $events = $erepo->findBy([], [ 'startAt' => 'DESC']);

        return $this->render('main/club/la_salle.html.twig', [
            'news' => $lastNews,
            'events' => $events
        ]);
    }

    #[Route('/contacter-le-club', name: 'app_contact')]
    public function contact(Request $request, NewsRepository $nrepo, EventRepository $erepo)
    {
        $lastNews = $nrepo->findBy([], ['createdAt' => 'DESC'], 4);
        $events = $erepo->findBy([], [ 'startAt' => 'DESC']);

        return $this->render('main/club/contact.html.twig', [
            'news' => $lastNews,
            'events' => $events
        ]);
    }
}
