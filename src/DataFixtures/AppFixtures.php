<?php

namespace App\DataFixtures;

use App\Entity\Event;
use App\Entity\News;
use App\Entity\User;
use DateTimeImmutable;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use PhpParser\Node\Expr\Cast\String_;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasher;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        // $product = new Product();
        // $manager->persist($product);


        for ($i=0; $i < 10; $i++) { 
            $new = new News();
            $new->setTitle("news n ".strval($i));
            $new->setContent("lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum ");
            $new->setCreatedAt(new DateTimeImmutable());
            $new->setSlug("n".strval($i));
            $manager->persist($new);
        }

        for ($i=0; $i < 10; $i++) { 
            $new = new Event();
            $new->setTitle("news n ".strval($i));
            $new->setContent("lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum ");
            $new->setCreatedAt(new DateTimeImmutable());
            $new->setStartAt(new DateTimeImmutable());
            $manager->persist($new);
        }


        $manager->flush();
    }
}
